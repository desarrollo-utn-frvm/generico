package logisoft.generico.web.genericos;

import java.io.Serializable;
import java.util.Map;


import logisoft.generico.dto.Cantidad;
import logisoft.generico.logica.ServicioGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


public class ControladorGenerico<T, ID extends Serializable> {

	@Autowired
	protected ServicioGenerico<T, ID> servicioGenerico;

	@RequestMapping("/{id}")
	public T get(@PathVariable("id") ID id) {
		return servicioGenerico.getItem(id);
	}

	// @RequestMapping(params = { "pagina", "cantidad" })
	// public Iterable<T> get(
	// @RequestParam("pagina") int pagina,
	// @RequestParam("cantidad") int cantidad,
	// @RequestParam(value = "filtro", defaultValue = "") String filtro,
	// @RequestParam(value = "campoOrden", defaultValue = "id") String
	// campoOrden,
	// @RequestParam(value = "reversoOrden", defaultValue = "false") boolean
	// reversoOrden) {
	// return servicioGenerico.listarPaginado(pagina, cantidad, filtro,
	// campoOrden, reversoOrden);
	// }

	@RequestMapping
	public Iterable<T> getPagina(@RequestParam Map<String, String> parametros) {
		if (parametros.isEmpty())
			return servicioGenerico.listarTodos();
		return servicioGenerico.listarFiltrado(parametros);
	}

	@RequestMapping(params = { "sort" })
	public Iterable<T> getPagina(@RequestParam Map<String, String> parametros,
			Pageable pagina) {
		parametros.remove("sort");
		parametros.remove("size");
		parametros.remove("page");
		boolean paginacion = parametros.containsKey("pagination");
		parametros.remove("pagination");
		Iterable<T> respuesta;
		if (parametros.isEmpty()) {
			respuesta = servicioGenerico.listarPaginado(pagina);
		} else {
			respuesta = servicioGenerico.listarFiltradoYPaginado(parametros,
					pagina);
		}
		if (!paginacion) {
			return ((Page<T>)respuesta).getContent();
		} else {
			return respuesta;
		}
	}

	@RequestMapping(value = "/cantidad")
	public Cantidad getCantidadClientes() {
		return new Cantidad(servicioGenerico.getCantidad());
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public T eliminar(@PathVariable("id") ID id) {
		return servicioGenerico.eliminar(id);
	}

}
