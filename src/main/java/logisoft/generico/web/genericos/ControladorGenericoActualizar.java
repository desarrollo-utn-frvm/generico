package logisoft.generico.web.genericos;

import java.io.Serializable;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class ControladorGenericoActualizar<T, ID extends Serializable> extends
		ControladorGenerico<T, ID> {

	@RequestMapping(method = RequestMethod.POST)
	public T guardar(@RequestBody T p) throws Exception {
		return servicioGenerico.guardar(p);
	}

	@RequestMapping(method = RequestMethod.PUT, value="/{id}")
	public T actualizar(@RequestBody T p, Long id) throws Exception {
		return servicioGenerico.actualizarItem(p);
	}
}
