package logisoft.generico.web.soporte;

import java.lang.reflect.Method;
import java.util.ArrayList;

import logisoft.generico.dominio.ObjetoDominio;
import logisoft.generico.dominio.soporte.reporte.ObjetoColumnaReporte;
import logisoft.generico.dominio.soporte.reporte.ObjetoReporte;
import logisoft.generico.logica.reporte.ServicioReporte;
import logisoft.generico.web.genericos.ControladorGenericoActualizar;
import lombok.Data;
import lombok.NonNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

@RestController
@RequestMapping("/reportes")
public class ControladorReportes extends
		ControladorGenericoActualizar<ObjetoReporte, Long> {

	@Autowired
	private ServicioReporte servicio;

	@JsonView(ObjetoReporte.Vistas.Completa.class)
	@RequestMapping("/{id}")
	public ObjetoReporte get(@PathVariable("id") Long id) {
		return servicio.getItem(id);
	}

	@RequestMapping(value = "/{id}/detalle")
	public Iterable<ObjetoColumnaReporte> listarDetalleAcuerdoComerciales(
			@PathVariable("id") Long id) {
		return servicio.listarDetalles(id);
	}

	@RequestMapping(value = "/{id}/detalle", method = RequestMethod.POST)
	public boolean guardarDetalleReporte(@PathVariable("id") Long id,
			@RequestBody ObjetoColumnaReporte nuevoDetalleReporte) {
		servicio.guardarDetalleAcuerdoComercial(id, nuevoDetalleReporte);
		return true;
	}

	@RequestMapping(value = "/{id}/detalle", method = RequestMethod.PUT)
	public boolean actualizarDetalleReporte(@PathVariable("id") Long id,
			@RequestBody ObjetoColumnaReporte actualizarDetalleReporte) {
		servicio.actualizarDetalleAcuerdoComercial(id, actualizarDetalleReporte);
		return true;
	}

	@RequestMapping(value = "/{idReporte}/detalle/{idDetalle}", method = RequestMethod.DELETE)
	public boolean eliminarDetalleAcuerdoComercial(
			@PathVariable("idReporte") Long idReporte,
			@PathVariable("idDetalle") Long idDetalle) {
		servicio.eliminarDetalleAcuerdoComercial(idReporte, idDetalle);
		return true;
	}

	@RequestMapping(value = "/metodos")
	public ArrayList<MetodoDTO> getMetodosDeLaClase(
			@RequestParam(value = "clase") Class<?> clase,
			@RequestParam(value = "filtro") String filtro) {
		Method[] methods = clase.getMethods();
		ArrayList<MetodoDTO> metodos = new ArrayList<MetodoDTO>();
		for (Method m : methods) {
			if (!m.getName().startsWith("get")
					|| !m.getName().toUpperCase()
							.contains(filtro.toUpperCase())
					|| m.getName().contains("Class"))
				continue;
			if (ObjetoDominio.class.isAssignableFrom(m.getReturnType())) {
				for (Method subM : m.getReturnType().getMethods()) {
					if (!subM.getName().startsWith("get")
							|| !subM.getName().toUpperCase()
									.contains(filtro.toUpperCase())
							|| m.getName().contains("Class"))
						continue;
					metodos.add(new MetodoDTO(m.getName() + "()."
							+ subM.getName() + "()"));
				}
			}

			metodos.add(new MetodoDTO(m.getName() + "()"));
		}
		return metodos;
	}

	@Data
	private class MetodoDTO {
		@NonNull
		private String nombre;
	}

}
