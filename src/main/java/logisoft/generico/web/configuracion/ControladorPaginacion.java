package logisoft.generico.web.configuracion;

import logisoft.generico.dominio.web.configuracion.Paginacion;
import logisoft.generico.logica.configuracion.ServicioPaginacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/configuracion/paginacion")
public class ControladorPaginacion {

	@Autowired
	private ServicioPaginacion servicio;

	@RequestMapping
	public Paginacion getPaginacion() {
		return servicio.getPaginacion();
	}

}
