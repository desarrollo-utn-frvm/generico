package logisoft.generico.dominio;

import javax.persistence.Entity;
import javax.persistence.Lob;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ObjetoImagen extends ObjetoDominio {

	@Lob
	@NonNull
	@NotEmpty
	private byte[] imagen;
	@NotBlank
	@NonNull
	private String tipoImagen;
	private Long idObjeto;
	private String nombreClase;
}
