package logisoft.generico.dominio.soporte.reporte;

public enum AlineacionCampoReporte {

	IZQUIERDA("Left"), JUSTIFICADO("Justified"), CENTRADO("Center"), DERECHA(
			"Right");

	private String alineacion;

	private AlineacionCampoReporte(String alineacion) {
		this.alineacion = alineacion;
	}

	public String getAlineacion() {
		return alineacion;
	}

	public void setAlineacion(String alineacion) {
		this.alineacion = alineacion;
	}

	public String toString() {
		return alineacion;
	}

	

}
