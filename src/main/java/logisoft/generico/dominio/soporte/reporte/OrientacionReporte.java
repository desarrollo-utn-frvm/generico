package logisoft.generico.dominio.soporte.reporte;

public enum OrientacionReporte {
	VERTICAL("Portrait", "595", "842", "555"), HORIZONTAL("Landscape", "842", "595", "802");

	private String nombre;
	private String ancho;
	private String alto;
	private String anchoColumna;

	private OrientacionReporte(String nombre, String ancho, String alto,
			String anchoColumna) {
		this.nombre = nombre;
		this.ancho = ancho;
		this.alto = alto;
		this.anchoColumna = anchoColumna;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAncho() {
		return ancho;
	}

	public void setAncho(String ancho) {
		this.ancho = ancho;
	}

	public String getAlto() {
		return alto;
	}

	public void setAlto(String alto) {
		this.alto = alto;
	}

	public String getAnchoColumna() {
		return anchoColumna;
	}

	public void setAnchoColumna(String anchoColumna) {
		this.anchoColumna = anchoColumna;
	}

}
