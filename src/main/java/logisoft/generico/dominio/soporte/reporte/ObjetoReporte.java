package logisoft.generico.dominio.soporte.reporte;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonView;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class ObjetoReporte extends ObjetoDominio {

	@Column(unique = true)
	private String nombreReporte;
	private String titulo;
	@Transient
	private String filtro;
	@Enumerated(EnumType.STRING)
	private OrientacionReporte orientacion;
	private Class<?> clase;
	@JsonView({ ObjetoReporte.Vistas.Completa.class })
	@ManyToMany(cascade = CascadeType.ALL)
	@OrderColumn(name = "pos")
	private List<ObjetoColumnaReporte> columnas = new ArrayList<ObjetoColumnaReporte>();
	private Integer alturaBandaDetalle;

	public static class Vistas {
		public static class Completa {

		}
	}

}
