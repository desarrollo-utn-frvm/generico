package logisoft.generico.dominio.soporte.reporte;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ObjetoColumnaReporte extends ObjetoDominio {
	@NonNull
	private Integer ancho;
	@NonNull
	private String metodo;
	@NonNull
	private String tituloEncabezado;
	@NonNull
	@Enumerated(EnumType.STRING)
	private AlineacionCampoReporte alineacionCampo;
	@NonNull
	private Integer tamanioFuente;

}
