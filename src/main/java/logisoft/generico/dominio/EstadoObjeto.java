package logisoft.generico.dominio;

public enum EstadoObjeto {
	
	BAJA,
	HABILITADO;

}
