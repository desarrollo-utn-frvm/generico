package logisoft.generico.dominio;

import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonView;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode(callSuper=true, of={})
@Data
public class ObjetoAuditable extends ObjetoDominio {

	@Getter(onMethod = @__({ @JsonView(ObjetoAuditable.Vistas.Auditoria.class) }))
	@CreatedDate
	protected DateTime fechaCreacion;
	@Getter(onMethod = @__({ @JsonView(ObjetoAuditable.Vistas.Auditoria.class) }))
	@LastModifiedDate
	protected DateTime fechaModificacion;
	@Getter(onMethod = @__({ @JsonView(ObjetoAuditable.Vistas.Auditoria.class) }))
	@CreatedBy
	protected String usuarioCreacion;
	@Getter(onMethod = @__({ @JsonView() }))
	@LastModifiedBy
	protected String usuarioModificacion;

	public static class Vistas {
		public static class Auditoria {

		}

		public static class Defecto {

		}
	}
}
