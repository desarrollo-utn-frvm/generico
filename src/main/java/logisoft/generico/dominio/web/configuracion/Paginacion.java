package logisoft.generico.dominio.web.configuracion;

import javax.persistence.Entity;

import logisoft.generico.dominio.ObjetoDominio;



@Entity
public class Paginacion extends ObjetoDominio {
	private int itemsPorPagina;
	private int tamanioMaximo;
	private int itemsPorPaginaTransitorio;

	public Paginacion() {
		super();
	}

	public int getItemsPorPagina() {
		return itemsPorPagina;
	}

	public void setItemsPorPagina(int itemsPorPagina) {
		this.itemsPorPagina = itemsPorPagina;
	}

	public int getTamanioMaximo() {
		return tamanioMaximo;
	}

	public void setTamanioMaximo(int tamanioMaximo) {
		this.tamanioMaximo = tamanioMaximo;
	}

	public int getItemsPorPaginaTransitorio() {
		return itemsPorPaginaTransitorio;
	}

	public void setItemsPorPaginaTransitorio(int itemsPorPaginaTransitorio) {
		this.itemsPorPaginaTransitorio = itemsPorPaginaTransitorio;
	}

}
