package logisoft.generico.dominio;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@MappedSuperclass
@Data
public class ObjetoDominio {
	
	@Id @GeneratedValue
	private Long id;

}
