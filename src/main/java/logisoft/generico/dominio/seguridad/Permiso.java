package logisoft.generico.dominio.seguridad;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import logisoft.generico.dominio.ObjetoAuditable;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity @Data @EqualsAndHashCode(callSuper=true,of="nombre") @Audited
@RequiredArgsConstructor @NoArgsConstructor(access=AccessLevel.PRIVATE)
public class Permiso extends ObjetoAuditable implements GrantedAuthority {

	private static final long serialVersionUID = 1L;

	@NotBlank @NonNull @Column(unique=true)
	private String nombre;
	
	@NotNull @NonNull
	private String descripcion;
	
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Permiso> padres = new HashSet<Permiso>();

	
	
	@JsonIgnore
	public Set<Permiso> getAncestros() {
		Set<Permiso> ancestros = new HashSet<Permiso>();
		ancestros.add(this);
		for (Permiso p : padres) {
			ancestros.addAll(p.getAncestros());
		}
		return ancestros;
	}


	public String getAuthority() {
		return nombre;
	}
	
}
