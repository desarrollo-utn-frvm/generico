package logisoft.generico.dominio.seguridad;

import lombok.Data;
import lombok.Value;

import org.joda.time.DateTime;

@Value @Data
public class Token {
	
	private Usuario usuario;
	
	private String token;
	
	private DateTime fechaExpiracion;

	public void test() {
		String nombre = getToken();
	}
}
