package logisoft.generico.dominio.seguridad;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import logisoft.generico.dominio.ObjetoAuditable;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity @Data @EqualsAndHashCode(callSuper=true,of="nombre") @Audited
@RequiredArgsConstructor @NoArgsConstructor(access=AccessLevel.PRIVATE)
public class Rol extends ObjetoAuditable {
	
	@NotBlank @NonNull @Column(unique=true)
	private String nombre;
	
	@NotNull @NonNull
	private String descripcion;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@Valid @NotNull @NonNull
	private Set<Permiso> permisos = new HashSet<Permiso>();
	
	@JsonIgnore
	public Set<Permiso> getPermisosAnidados() {
		Set<Permiso> anidados = new HashSet<Permiso>();
		for (Permiso p : permisos) {
			anidados.addAll(p.getAncestros());
		}
		return anidados;
	}

	public boolean esVendedor() {
		return "Vendedor".equals(getNombre());
	}

}
