package logisoft.generico.dominio.seguridad;

import java.util.Date;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class AuditoriaIngreso extends ObjetoDominio {

	@NonNull @NotNull
	private String url;
	@NonNull @NotNull
	private Date fecha;
	@NonNull @NotNull
	private String usuario;
	@NonNull @NotNull
	private Integer estado;
	@NonNull @NotNull
	private String ip;
	
}
