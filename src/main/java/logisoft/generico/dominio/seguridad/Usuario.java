package logisoft.generico.dominio.seguridad;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import logisoft.generico.dominio.ObjetoAuditable;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;


@Entity @Data @EqualsAndHashCode(callSuper=true,of={"nombre"}) @Audited
@RequiredArgsConstructor @NoArgsConstructor(access=AccessLevel.PRIVATE)
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"nombre"}))
public class Usuario extends ObjetoAuditable implements UserDetails {
	
	private static final long serialVersionUID = 1L;
	
	@NotBlank @NonNull
	@Pattern(regexp = "^[A-Za-z0-9_-]+$") @Size(min=5)
	private String nombre;
	
	@JsonView({ObjetoAuditable.Vistas.Defecto.class, Vistas.Actual.class})
	@NotBlank @NonNull @Size(min=5)
	private String password;

	@Email
	private String email;
	
	@JsonView({ObjetoAuditable.Vistas.Defecto.class, Vistas.Actual.class})
	@NotNull @NonNull @Enumerated(EnumType.STRING)
	private EstadoUsuario estado = EstadoUsuario.SIN_CONFIRMAR;
	
	@JsonView({ObjetoAuditable.Vistas.Defecto.class, Vistas.Actual.class})
	@ManyToMany
	//(cascade={CascadeType.PERSIST, CascadeType.MERGE}, 
			(fetch=FetchType.EAGER)
	@Valid @NotNull @NonNull
	private Set<Rol> roles = new HashSet<Rol>();
	

	@JsonView({ObjetoAuditable.Vistas.Defecto.class, Vistas.Actual.class})
	@NotNull @NonNull
	private Boolean habilitado = true;
	
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<Permiso> permisos = new HashSet<Permiso>();
		for (Rol r : getRoles()) {
			permisos.addAll(r.getPermisos());
		}
		return permisos;
	}
	
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return getHabilitado();
	}

	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@JsonIgnore
	public boolean isEnabled() {
		return getHabilitado();
	}
	
	public class Vistas {
		public class Actual {}
	}


	public String getUsername() {
		return this.nombre;
	}

}
