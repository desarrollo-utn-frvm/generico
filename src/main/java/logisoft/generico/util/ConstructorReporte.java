package logisoft.generico.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ConstructorReporte {

	private Resource logo;

	Map<Resource, JasperReport> cache;

	public ConstructorReporte(Resource logo) {
		this.logo = logo;
		cache = new HashMap<Resource, JasperReport>();
	}

	public JasperReport getReporte(Resource recurso) throws IOException,
			JRException {
		if (!cache.containsKey(recurso)) {
			JasperDesign jasperDesign = JRXmlLoader.load(recurso
					.getInputStream());
			// compile the jasperDesign
			cache.put(recurso, JasperCompileManager.compileReport(jasperDesign));
		}
		return cache.get(recurso);
	}

	public byte[] ejecutarReportePDF(JasperReport reporte,
			Map<String, Object> parametros, JRBeanCollectionDataSource datos) {
		try {
			Locale l = new Locale("es", "AR");
			parametros.put(JRParameter.REPORT_LOCALE, l);
			parametros.put("logo", logo.getInputStream());
			return JasperRunManager.runReportToPdf(reporte, parametros, datos);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
