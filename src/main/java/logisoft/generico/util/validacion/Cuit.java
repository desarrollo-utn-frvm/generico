package logisoft.generico.util.validacion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;

@Documented
@Constraint(validatedBy={})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Pattern(regexp="[0-9]{2,2}-[0-9]{7,8}-[0-9]")
public @interface Cuit {
	public abstract String message() default "{validacion.cuit.mensaje}";
	public abstract Class<?>[] groups() default {};
	public Class<? extends Payload>[] payload() default { };
	
}
