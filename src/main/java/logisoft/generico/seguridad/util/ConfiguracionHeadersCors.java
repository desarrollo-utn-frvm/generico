package logisoft.generico.seguridad.util;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration 
@Data
@ConfigurationProperties(prefix="seguridad.cors")
public class ConfiguracionHeadersCors {
	
	private Map<String,String> headers = new HashMap<String, String>();

}
