package logisoft.generico.seguridad.util;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationUtil {

    private AuthenticationUtil() {
    }
 
    public static void clearAuthentication() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }
 
    public static void configureAuthentication(String nombreUsuario, String... roles) {
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(roles);
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                nombreUsuario,
                roles,
                authorities
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
	
}
