package logisoft.generico.seguridad.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration @Data
@ConfigurationProperties(prefix="seguridad.roles")
public class ConfiguracionPermisosAplicacion {

	private Map<String,List<String>> roles = new HashMap<String,List<String>>();
	
}