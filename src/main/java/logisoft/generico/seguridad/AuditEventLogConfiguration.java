package logisoft.generico.seguridad;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AuditEventLogConfiguration implements ApplicationListener<AuthenticationSuccessEvent> {
	
	public AuditEventLogConfiguration() {
		super();
	}
	
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		log.info(event.toString());
	}

}
