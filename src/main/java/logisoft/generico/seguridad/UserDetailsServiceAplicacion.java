package logisoft.generico.seguridad;

import logisoft.generico.dominio.seguridad.Usuario;
import logisoft.generico.persistencia.RepositorioUsuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsServiceAplicacion implements UserDetailsService {

	private RepositorioUsuario utilUsuario;

	@Autowired
	public UserDetailsServiceAplicacion(RepositorioUsuario utilUsuario) {
		super();
		this.utilUsuario = utilUsuario;
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Usuario usuario = utilUsuario.findByNombre(username);

		if (usuario == null) {
			throw new UsernameNotFoundException("El usuario " + username
					+ " no existe");
		}
		return usuario;
	}

}
