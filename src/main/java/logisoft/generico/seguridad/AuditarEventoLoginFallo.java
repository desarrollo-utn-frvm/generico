package logisoft.generico.seguridad;

import logisoft.generico.logica.seguridad.ServicioAuditoriaIngreso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;


@Component
public class AuditarEventoLoginFallo implements
		ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

	@Autowired
	private ServicioAuditoriaIngreso servicio;

	public AuditarEventoLoginFallo() {
		super();
	}

	public void onApplicationEvent(
			AuthenticationFailureBadCredentialsEvent event) {
		UsernamePasswordAuthenticationToken source = (UsernamePasswordAuthenticationToken) event.getSource();
		WebAuthenticationDetails detalles = (WebAuthenticationDetails) source.getDetails();
		servicio.registrarInicioSesion("/login", "GET", event
				.getAuthentication().getName(), 401, detalles.getRemoteAddress());
	}

}
