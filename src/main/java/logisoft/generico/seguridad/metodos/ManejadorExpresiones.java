package logisoft.generico.seguridad.metodos;

import logisoft.generico.logica.seguridad.ServicioPermisosIniciales;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

public class ManejadorExpresiones extends DefaultMethodSecurityExpressionHandler{

	private AuthenticationTrustResolver trustResolver;
	
	private ServicioPermisosIniciales servicioPermisosIniciales;
	
	public ManejadorExpresiones(ServicioPermisosIniciales servicio) {
		super();
		setTrustResolver(new AuthenticationTrustResolverImpl());
		this.servicioPermisosIniciales = servicio;
	}

	@Override
	protected MethodSecurityExpressionOperations createSecurityExpressionRoot(
			Authentication authentication, MethodInvocation invocation) {
		RaizExpresiones root = new RaizExpresiones(authentication, servicioPermisosIniciales);
        root.setThis(invocation.getThis());
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        return root;
	}

	public void setTrustResolver(AuthenticationTrustResolver trustResolver) {
		this.trustResolver = trustResolver;
		super.setTrustResolver(trustResolver);
	}

}
