package logisoft.generico.seguridad.metodos;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import logisoft.generico.dominio.seguridad.Permiso;
import logisoft.generico.logica.seguridad.ServicioPermisosIniciales;

import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

public class RaizExpresiones extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {
	
	private Pattern prefijo = Pattern.compile("ar\\.com\\.carlospazentumano\\.negociosMovil\\.logica\\.(.*)");
	
    private Object filterObject;
    private Object returnObject;
    private Object target;
    
    private ServicioPermisosIniciales servicioPermisosIniciales;

    RaizExpresiones(Authentication a, ServicioPermisosIniciales servicioPermisosIniciales) {
        super(a);
        this.servicioPermisosIniciales = servicioPermisosIniciales;
    }

    public void setFilterObject(Object filterObject) {
        this.filterObject = filterObject;
    }

    public Object getFilterObject() {
        return filterObject;
    }

    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }

    public Object getReturnObject() {
        return returnObject;
    }

    public boolean tienePermiso() {
		return tienePermisoInterno(getThis().getClass().getName());
	}

    public boolean tienePermiso(String nombrePermiso) {
    	return tienePermisoInterno(getThis().getClass().getName()+"."+nombrePermiso);		
	}
    
    private boolean tienePermisoInterno(String nombrePermiso) {
    	Matcher matcher = prefijo.matcher(nombrePermiso);
    	matcher.matches();
    	if (matcher.groupCount() < 1)
    		return false;
    	String nombreCorto = matcher.group(1);
    	Permiso permiso = servicioPermisosIniciales.buscarPorNombre(nombreCorto);
		if (permiso != null) {
			return tienePermiso(permiso);
		} else {
			return false;
		}
    }
    
    private boolean tienePermiso(Permiso permiso) {
    	if (hasRole(permiso.getNombre())) {
    		return true;
    	} else {
    		for (Permiso p : permiso.getPadres()) {
    			if (tienePermiso(p)) return true;
    		}
    		return false;
    	}
    }

	/**
     * Sets the "this" property for use in expressions. Typically this will be the "this" property of
     * the {@code JoinPoint} representing the method invocation which is being protected.
     *
     * @param target the target object on which the method in is being invoked.
     */
    void setThis(Object target) {
        this.target = target;
    }

    public Object getThis() {
        return target;
    }
}
