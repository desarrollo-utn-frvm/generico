package logisoft.generico.seguridad.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.google.common.base.Optional;

public class ProveedorAutenticacionToken implements AuthenticationProvider {

    private ServicioAlmacenAutenticaciones servicioToken;

    @Autowired
    public ProveedorAutenticacionToken(ServicioAlmacenAutenticaciones servicioToken) {
        this.servicioToken = servicioToken;
    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Optional<String> token = Optional.fromNullable(String.valueOf(authentication.getPrincipal()));
        if (!token.isPresent() || token.get().isEmpty()) {
            throw new BadCredentialsException("Invalid token");
        }
        if (!servicioToken.contiene(token.get())) {
            throw new BadCredentialsException("Invalid token or token expired");
        }
        return servicioToken.recuperarAutenticacion(token.get());
    }

    public boolean supports(Class<?> authentication) {
        return authentication.equals(PreAuthenticatedAuthenticationToken.class);
    }
}