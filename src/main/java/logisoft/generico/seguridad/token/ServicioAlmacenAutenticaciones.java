package logisoft.generico.seguridad.token;

import java.util.UUID;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import lombok.extern.slf4j.Slf4j;

@Slf4j @Service
public class ServicioAlmacenAutenticaciones {

    private static final Cache restApiAuthTokenCache = CacheManager.getInstance().getCache("restApiAuthTokenCache");
    public static final int HALF_AN_HOUR_IN_MILLISECONDS = 30 * 60 * 1000;

    @Scheduled(fixedRate = HALF_AN_HOUR_IN_MILLISECONDS)
    public void limpiarTokensViejos() {
        log.info("Limpiando tokens viejos");
        restApiAuthTokenCache.evictExpiredElements();
    }

    public String crearNuevoToken() {
        return UUID.randomUUID().toString();
    }

    public void guardarAutenticacion(String token, Authentication authentication) {
        restApiAuthTokenCache.put(new Element(token, authentication));
    }

    public boolean contiene(String token) {
        return restApiAuthTokenCache.get(token) != null;
    }

    public Authentication recuperarAutenticacion(String token) {
        return (Authentication) restApiAuthTokenCache.get(token).getObjectValue();
    }
}
