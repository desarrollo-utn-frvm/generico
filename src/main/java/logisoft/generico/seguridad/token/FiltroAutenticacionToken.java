package logisoft.generico.seguridad.token;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logisoft.generico.dominio.seguridad.Usuario;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;

import com.google.common.base.Optional;

@Slf4j
public class FiltroAutenticacionToken extends GenericFilterBean {

    private AuthenticationManager authenticationManager;

    @Autowired
    public FiltroAutenticacionToken(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = asHttp(request);
        HttpServletResponse httpResponse = asHttp(response);
        Optional<String> token = Optional.fromNullable(httpRequest.getHeader("X-Auth-Token"));
        intentarProcesar(httpRequest, httpResponse, chain, token);
    }

	private void intentarProcesar(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain, Optional<String> token)
			throws IOException, ServletException {
		try {
            procesar(request, response, chain, token);
        } catch (InternalAuthenticationServiceException ex) {
        	arrojarError(ex, response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error al intentar autenticar.");
        } catch (AuthenticationException ex) {
        	arrojarError(ex, response, HttpServletResponse.SC_UNAUTHORIZED, "Error de autenticación.");
        }
	}

	private void procesar(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain,
			Optional<String> token) throws IOException, ServletException {
		intentarAutenticar(token);
		log.debug("FiltroAutenticacionToken pasando mensaje al siguiente filtro");
		chain.doFilter(request, response);
	}

	private void arrojarError(Exception ex, HttpServletResponse httpResponse, int estado, String mensaje)
			throws IOException {
		log.error(mensaje, ex);
		SecurityContextHolder.clearContext();
		httpResponse.sendError(estado, ex.getMessage());
	}

	private void intentarAutenticar(Optional<String> token) {
		if (token.isPresent()) {
		    log.info("Intentando autenticar al usuario mediante token. Token: {}", token);
		    intentarAutenticar(token.get());
		}
	}

    private HttpServletRequest asHttp(ServletRequest request) {
        return (HttpServletRequest) request;
    }

    private HttpServletResponse asHttp(ServletResponse response) {
        return (HttpServletResponse) response;
    }

    private void intentarAutenticar(String token) {
        Authentication resultOfAuthentication = autenticar(token);
        guardarAutenticacion(resultOfAuthentication);
    }

	private void guardarAutenticacion(Authentication resultOfAuthentication) {
		SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
	}

    private Authentication autenticar(String token) {
        PreAuthenticatedAuthenticationToken requestAuthentication = new PreAuthenticatedAuthenticationToken(token, null);
        return intentarAutenticar(requestAuthentication);
    }

    private Authentication intentarAutenticar(Authentication autenticacionInicial) {
        Authentication autenticacionValidada = authenticationManager.authenticate(autenticacionInicial);
        if (autenticacionValidada == null || !autenticacionValidada.isAuthenticated()) {
            throw new InternalAuthenticationServiceException("Error de autenticación");
        }
        log.debug("Usuario autenticado mediante autenticacion por token. Nombre de usuario: {}", ((Usuario)autenticacionValidada.getPrincipal()).getNombre());
        return autenticacionValidada;
    }
    
}
