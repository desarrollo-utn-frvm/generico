package logisoft.generico.seguridad.token;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class FiltroAsignacionToken extends GenericFilterBean  {
	
	private ServicioAlmacenAutenticaciones servicio;
	
	@Autowired
	public FiltroAsignacionToken(ServicioAlmacenAutenticaciones servicio) {
		this.servicio = servicio;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		agregarToken(asHttp(request), asHttp(response));
		chain.doFilter(request, response);		
	}
	
	private void agregarToken(HttpServletRequest request, HttpServletResponse response) {
    	Authentication auth = obtenerAutenticacion();
    	if (tieneToken(request)) return;
    	if (autenticacionValida(auth)) {
    		agregarToken(response, generarToken(auth));
    	}
    }

	private void agregarToken(HttpServletResponse response, String token) {
		response.addHeader("X-Auth-Token", token);
	}

	private Authentication obtenerAutenticacion() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth;
	}

	private boolean tieneToken(HttpServletRequest request) {
		return request.getHeader("X-Auth-Token") != null;
	}

	private String generarToken(Authentication auth) {
		String token = servicio.crearNuevoToken();
		servicio.guardarAutenticacion(token, auth);
		return token;
	}

	private boolean autenticacionValida(Authentication auth) {
		return auth != null && auth.isAuthenticated() && auth instanceof UsernamePasswordAuthenticationToken;
	}

    private HttpServletRequest asHttp(ServletRequest request) {
        return (HttpServletRequest) request;
    }

    private HttpServletResponse asHttp(ServletResponse response) {
        return (HttpServletResponse) response;
    }

}
