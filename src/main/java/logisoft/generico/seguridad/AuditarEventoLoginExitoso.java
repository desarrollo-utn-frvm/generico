package logisoft.generico.seguridad;

import logisoft.generico.logica.seguridad.ServicioAuditoriaIngreso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.ServletRequestHandledEvent;

@Component
public class AuditarEventoLoginExitoso implements
		ApplicationListener<ServletRequestHandledEvent> {

	@Autowired
	private ServicioAuditoriaIngreso servicio;

	public AuditarEventoLoginExitoso() {
		super();
	}

	public void onApplicationEvent(ServletRequestHandledEvent event) {
		if (event.getRequestUrl().equalsIgnoreCase("/login")) {
			servicio.registrarInicioSesion(event.getRequestUrl(),
					event.getMethod(), event.getUserName(),
					event.getStatusCode(), event.getClientAddress());
		}
	}

}
