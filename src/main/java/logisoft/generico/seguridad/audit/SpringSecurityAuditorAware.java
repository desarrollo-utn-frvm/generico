package logisoft.generico.seguridad.audit;

import logisoft.generico.dominio.seguridad.Usuario;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

	public String getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    if (authentication == null) {
	    	return ":sistema";
	    } else if (!authentication.isAuthenticated()) { 
	      return ":anonimo";
	    }
	    if (authentication.getPrincipal() instanceof String) {
	    	return ((String)authentication.getPrincipal());
	    }
	    return ((Usuario) authentication.getPrincipal()).getNombre();
	}
	
}
