package logisoft.generico.persistencia.configuracion;

import logisoft.generico.dominio.web.configuracion.Paginacion;
import logisoft.generico.persistencia.RepositorioGenerico;



public interface RepositorioPaginacion extends RepositorioGenerico<Paginacion, Long> {

}
