package logisoft.generico.persistencia;

import logisoft.generico.dominio.ObjetoImagen;

import org.springframework.stereotype.Repository;


@Repository
public interface RepositorioImagen extends RepositorioGenerico<ObjetoImagen, Long>{
	
	public ObjetoImagen findByNombreClaseAndIdObjeto(String clase, Long id);

}
