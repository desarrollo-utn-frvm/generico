package logisoft.generico.persistencia;

import logisoft.generico.dominio.seguridad.Permiso;

import org.springframework.data.repository.CrudRepository;


public interface RepositorioPermiso extends RepositorioGenerico<Permiso, Long> {

	public Permiso findByNombre(String nombre);

}
