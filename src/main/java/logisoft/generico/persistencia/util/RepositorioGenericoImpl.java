package logisoft.generico.persistencia.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.xml.bind.DatatypeConverter;

import logisoft.generico.dominio.ObjetoDominio;
import logisoft.generico.persistencia.RepositorioGenerico;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

public class RepositorioGenericoImpl<T, ID extends Serializable> extends
		SimpleJpaRepository<T, ID> implements RepositorioGenerico<T, ID> {

	private boolean ignorarAcentos;

	public RepositorioGenericoImpl(
			JpaEntityInformation<T, ?> entityInformation,
			EntityManager entityManager, boolean ignorarAcentos) {
		super(entityInformation, entityManager);
		this.ignorarAcentos = ignorarAcentos;
	}

	public RepositorioGenericoImpl(Class<T> domainClass, EntityManager em,
			boolean ignorarAcentos) {
		super(domainClass, em);
		this.ignorarAcentos = ignorarAcentos;
	}

	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros) {
		return buscarGenerico(filtros, getEstrategia(filtros));
	}

	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros,
			Sort orden) {
		return buscarGenerico(filtros, orden, getEstrategia(filtros));
	}

	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros,
			Pageable pagina) {
		return buscarGenerico(filtros, pagina, getEstrategia(filtros));
	}

	private String getEstrategia(Map<String, ? extends Object> filtros) {
		Object valor = filtros.get("strategy");
		if (valor != null && valor instanceof String) {
			filtros.remove("strategy");
			return valor.toString().toUpperCase();
		} else {
			return "OR";
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros,
			String estrategia) {
		Specifications especificaciones = crearEspecificaciones(filtros,
				Estrategia.valueOf(estrategia));
		return findAll(especificaciones);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros,
			Sort orden, String estrategia) {
		Specifications especificaciones = crearEspecificaciones(filtros,
				Estrategia.valueOf(estrategia));
		return findAll(especificaciones, orden);
	}

	private Set<Filtro> expandirPropiedades(
			Map<String, ? extends Object> filtros) {
		Set<Filtro> filtrosExpandidos = new HashSet<Filtro>();
		for (String clave : filtros.keySet()) {
			filtrosExpandidos.add(new Filtro(getDomainClass(), clave,
					(String) filtros.get(clave)));
		}
		return filtrosExpandidos;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Specifications crearEspecificaciones(
			Map<String, ? extends Object> filtros, Estrategia estrategia) {
		Specifications especificaciones = null;
		Set<Filtro> filtrosExpandidos = expandirPropiedades(filtros);
		Set<List<Filtro>> filtrosEmparejados = buscarPares(filtrosExpandidos);
		for (Filtro filtro : filtrosExpandidos) {
			if (especificaciones == null) {
				especificaciones = Specifications.where(filtro
						.getPredicado(ignorarAcentos));
			} else if (estrategia == Estrategia.AND) {
				especificaciones = especificaciones.and(filtro
						.getPredicado(ignorarAcentos));
			} else {
				especificaciones = especificaciones.or(filtro
						.getPredicado(ignorarAcentos));
			}
		}
		for (List<Filtro> l : filtrosEmparejados) {
			if (especificaciones == null) {
				especificaciones = Specifications
						.where(getEspecificacionesEmparejadas(l));
			} else if (estrategia == Estrategia.AND) {
				especificaciones = especificaciones
						.and(getEspecificacionesEmparejadas(l));
			} else {
				especificaciones = especificaciones
						.or(getEspecificacionesEmparejadas(l));
			}
		}
		return especificaciones;
	}

	private Specifications getEspecificacionesEmparejadas(List<Filtro> l) {
		if (l == null || l.size() != 2)
			return null;
		Specifications s = Specifications.where(l.get(0).getPredicado(
				ignorarAcentos));
		return s.and(l.get(1).getPredicado(ignorarAcentos));
	}

	private Set<List<Filtro>> buscarPares(Set<Filtro> filtrosExpandidos) {
		Set<List<Filtro>> pares = new HashSet<List<Filtro>>();
		Set<Filtro> eliminados = new HashSet<Filtro>();
		for (Filtro f : filtrosExpandidos) {
			if (eliminados.contains(f))
				continue;
			for (Filtro f2 : filtrosExpandidos) {
				if (eliminados.contains(f2))
					continue;
				if (f.esPareja(f2)) {
					pares.add(Arrays.asList(f, f2));
					eliminados.add(f);
					eliminados.add(f2);
				}
			}
		}
		filtrosExpandidos.removeAll(eliminados);
		return pares;
	}

	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros,
			Pageable pagina, String estrategia) {
		Specifications especificaciones = crearEspecificaciones(filtros,
				Estrategia.valueOf(estrategia));
		return findAll(especificaciones, pagina);
	}

	@Data
	@RequiredArgsConstructor(access = AccessLevel.NONE)
	private static class Filtro {

		private List<String> campo;

		private Class tipo;

		private String valor;

		private Modo modo;

		public Filtro(Class claseRaiz, String clave, String valor) {
			this.valor = valor;
			calcularModos(clave);
			asignarPropiedadAnidada(claseRaiz, campo);
		}

		public boolean esPareja(Filtro filtro) {
			if (!campo.equals(filtro.getCampo()))
				return false;
			return modo.esPareja(filtro.getModo());
		}

		private void calcularModos(String clave) {
			for (Modo m : Modo.values()) {
				calcularModo(m, clave);
			}
			if (modo == null) {
				modo = Modo.getModo("");
				asignarCampo(clave);
			}
		}

		private void calcularModo(Modo modo, String clave) {
			if (modo.getNombre().length() == 0)
				return;
			if (clave.indexOf(modo.getNombre()) == clave.length()
					- modo.getNombre().length()) {
				this.modo = modo;
				asignarCampo(clave.replace(modo.getNombre(), ""));
			}
		}

		private void asignarCampo(String clave) {
			String[] propiedades = clave.split("\\.");
			campo = new ArrayList<String>();
			for (String propiedad : propiedades) {
				if (!propiedad.equals("")) {
					campo.add(propiedad);
				}
			}
		}

		private void asignarPropiedadAnidada(Class claseRaiz,
				List<String> campos) {
			tipo = claseRaiz;
			for (String campo : campos) {
				if (!ObjetoDominio.class.isAssignableFrom(tipo)) {
					return;
				}
				tipo = ReflectionUtils.findField(tipo, campo).getType();
			}
		}

		public Specification<? extends ObjetoDominio> getPredicado(
				boolean ignorarAcentos) {
			if (tipo == null)
				return null;
			try {
				if (esString()) {
					return getPredicadoString(ignorarAcentos);
				} else if (esDouble()) {
					return getPredicadoDouble();
				} else if (esFloat()) {
					return getPredicadoFloat();
				} else if (esInteger()) {
					return getPredicadoInteger();
				} else if (esLong()) {
					return getPredicadoLong();
				} else if (esBigDecimal()) {
					return getPredicadoBigDecimal();
				} else if (esDate()) {
					return getPredicadoDate();
				} else if (esEnum()) {
					return getPredicadoEnum();
				} else if (esBoolean()){
					return getPredicadoBoolean();
				} else {
				
					return null;
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		private boolean esBoolean() throws ClassNotFoundException, LinkageError {
			return ClassUtils.forName("boolean", null).isAssignableFrom(tipo)
					|| Boolean.class.isAssignableFrom(tipo);
		}

		private boolean esDate() {
			return Date.class.isAssignableFrom(tipo);
		}

		private boolean esBigDecimal() {
			return BigDecimal.class.isAssignableFrom(tipo);
		}

		private boolean esInteger() throws ClassNotFoundException, LinkageError {
			return ClassUtils.forName("int", null).isAssignableFrom(tipo)
					|| Integer.class.isAssignableFrom(tipo);
		}

		private boolean esLong() throws ClassNotFoundException, LinkageError {
			return ClassUtils.forName("long", null).isAssignableFrom(tipo)
					|| Long.class.isAssignableFrom(tipo);
		}

		private boolean esFloat() throws ClassNotFoundException, LinkageError {
			return ClassUtils.forName("float", null).isAssignableFrom(tipo)
					|| Float.class.isAssignableFrom(tipo);
		}

		private boolean esDouble() throws ClassNotFoundException, LinkageError {
			return ClassUtils.forName("double", null).isAssignableFrom(tipo)
					|| Double.class.isAssignableFrom(tipo);
		}

		private boolean esString() {
			return tipo == String.class;
		}

		private boolean esEnum() {
			return Enum.class.isAssignableFrom(tipo);
		}

		private Specification<? extends ObjetoDominio> getPredicadoString(
				boolean ignorarAcentos) {
			return modo.getPredicadoString(campo, valor, ignorarAcentos);
		}
		
		private Specification<? extends ObjetoDominio> getPredicadoBoolean() {
			return modo.getPredicadoGenerico(campo, Boolean.valueOf(valor));
		}

		private Specification<? extends ObjetoDominio> getPredicadoDouble() {
			return modo.getPredicadoGenerico(campo, Double.valueOf(valor));
		}

		private Specification<? extends ObjetoDominio> getPredicadoInteger() {
			return modo.getPredicadoGenerico(campo, Integer.valueOf(valor));
		}

		private Specification<? extends ObjetoDominio> getPredicadoLong() {
			return modo.getPredicadoGenerico(campo, Long.valueOf(valor));
		}

		private Specification<? extends ObjetoDominio> getPredicadoFloat() {
			return modo.getPredicadoGenerico(campo, Float.valueOf(valor));
		}

		private Specification<? extends ObjetoDominio> getPredicadoBigDecimal() {
			return modo.getPredicadoGenerico(campo, new BigDecimal(valor));
		}

		private Specification<? extends ObjetoDominio> getPredicadoDate() {
			return modo.getPredicadoGenerico(campo, DatatypeConverter
					.parseDateTime(valor).getTime());
		}

		private Specification<? extends ObjetoDominio> getPredicadoEnum() {
			return modo.getPredicadoEnum(campo, valor, tipo);
		}

	}

	@Getter
	private static enum Modo {

		IGUAL(""), MENOR("<", ">", ">i"), MAYOR(">", "<", "<i"), MENOR_O_IGUAL(
				"<i", ">", ">i"), MAYOR_O_IGUAL(">i", "<", "<i"), IN("[]");

		private String nombre;

		private String[] opuestos;

		private Modo(String nombre, String... opuestos) {
			this.nombre = nombre;
			this.opuestos = opuestos;
		}

		public static Modo getModo(String nombre) {
			for (Modo m : Modo.values()) {
				if (m.getNombre().equals(nombre))
					return m;
			}
			return null;
		}

		public boolean esPareja(Modo modo) {
			for (String opuesto : opuestos) {
				if (opuesto.equals(modo.getNombre()))
					return true;
			}
			return false;
		}

		public Specification<? extends ObjetoDominio> getPredicadoString(
				List<String> campo, Comparable valor, boolean ignorarAcentos) {
			if (nombre.equals("<i")) {
				return Predicados.menorOIgual(campo, valor);
			} else if (nombre.equals(">i")) {
				return Predicados.mayorOIgual(campo, valor);
			} else if (nombre.equals("<")) {
				return Predicados.menor(campo, valor);
			} else if (nombre.equals(">")) {
				return Predicados.mayor(campo, valor);
			} else if (nombre.equals("[]")) {
				String[] valores = ((String) valor).split(";");
				return Predicados.in(campo, Arrays.asList(valores));
			} else {
				return Predicados.likeIgnoringCase(campo, "%" + (String) valor
						+ "%", ignorarAcentos);
			}
		}

		public Specification<? extends ObjetoDominio> getPredicadoEnum(
				List<String> campo, String valor, Class tipo) {
			if (nombre.equals("[]")) {
				String[] valores = ((String) valor).split(";");
				List<Enum> listaEnum = new ArrayList<Enum>();
				for (String v : valores) {
					listaEnum.add(Enum.valueOf(tipo, v));
				}
				return Predicados.in(campo, listaEnum);
			} else {
				return Predicados.esIgual(campo, Enum.valueOf(tipo, valor));
			}
		}

		public Specification<? extends ObjetoDominio> getPredicadoGenerico(
				List<String> campo, Comparable valor) {
			if (nombre.equals("<i")) {
				return Predicados.menorOIgual(campo, valor);
			} else if (nombre.equals(">i")) {
				return Predicados.mayorOIgual(campo, valor);
			} else if (nombre.equals("<")) {
				return Predicados.menor(campo, valor);
			} else if (nombre.equals(">")) {
				return Predicados.mayor(campo, valor);
			} else if (nombre.equals("[]")) {
				String[] valores = ((String) valor).split(";");
				return Predicados.in(campo, Arrays.asList(valores));
			} else {
				return Predicados.esIgual(campo, valor);
			}
		}

	}

	private enum Estrategia {
		AND, OR;
	}

}