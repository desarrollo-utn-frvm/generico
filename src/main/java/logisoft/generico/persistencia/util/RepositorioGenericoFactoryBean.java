package logisoft.generico.persistencia.util;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;


public class RepositorioGenericoFactoryBean<R extends JpaRepository<T, ID>, T, ID extends Serializable>
		extends JpaRepositoryFactoryBean<R, T, ID> {

	@Value("${config.busqueda.ignorarAcentos}")
	private boolean ignorarAcentos;

	protected RepositoryFactorySupport createRepositoryFactory(EntityManager em) {
		return new MyRepositoryFactory(em, ignorarAcentos);
	}

	private static class MyRepositoryFactory extends JpaRepositoryFactory {

		private final EntityManager em;
		private boolean ignorarAcentos;

		public MyRepositoryFactory(EntityManager em, boolean ignorarAcentos) {

			super(em);
			this.em = em;
			this.ignorarAcentos = ignorarAcentos;
		}

		protected Object getTargetRepository(RepositoryMetadata metadata) {
			return new RepositorioGenericoImpl(metadata.getDomainType(), em,
					ignorarAcentos);
		}

		protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
			return RepositorioGenericoImpl.class;
		}
	}
}
