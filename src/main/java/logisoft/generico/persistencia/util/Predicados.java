package logisoft.generico.persistencia.util;


import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import logisoft.generico.dominio.ObjetoDominio;

import org.springframework.data.jpa.domain.Specification;



public class Predicados {

	public static Specification<? extends ObjetoDominio> esIgual(
			final List<String> nombrePropiedades, final Object valor) {
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return cb.equal(nuevaRaiz, valor);
			}
		};
	}
	
	public static Specification<? extends ObjetoDominio> menor(
			final List<String> nombrePropiedades, final Comparable valor) {
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return cb.lessThan((Expression<Comparable>)nuevaRaiz, valor);
			}
		};
	}
	
	public static Specification<? extends ObjetoDominio> menorOIgual(
			final List<String> nombrePropiedades, final Comparable valor) {
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return cb.lessThanOrEqualTo((Expression<Comparable>)nuevaRaiz, valor);
			}
		};
	}
	
	public static Specification<? extends ObjetoDominio> mayor(
			final List<String> nombrePropiedades, final Comparable valor) {
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return cb.greaterThan((Expression<Comparable>)nuevaRaiz, valor);
			}
		};
	}
	
	public static Specification<? extends ObjetoDominio> mayorOIgual(
			final List<String> nombrePropiedades, final Comparable valor) {
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return cb.greaterThanOrEqualTo((Expression<Comparable>)nuevaRaiz, valor);
			}
		};
	}

	public static Specification<? extends ObjetoDominio> like(
			final List<String> nombrePropiedades, final String valor,
			final boolean ignorarAcentos) {
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return cb.like(filtrarSinAcentos(
						ignorarAcentos, cb, nuevaRaiz.as(String.class)), filtrarSinAcentos(
								ignorarAcentos, cb, cb.literal(valor)));
			}

		};
	}

	public static Specification<? extends ObjetoDominio> likeIgnoringCase(
			final List<String> nombrePropiedades, final String valor,
			final boolean ignorarAcentos) {
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return cb.like(filtrarSinAcentos(ignorarAcentos, cb, cb.upper(nuevaRaiz.as(String.class))),
						filtrarSinAcentos(ignorarAcentos, cb, cb.literal(valor.toUpperCase())));
			}

		};
	}

	private static Path<? extends Object> crearPath(
			final List<String> nombrePropiedades, Root<ObjetoDominio> root) {
		Path<? extends Object> nuevaRaiz = root;
		for (String propiedad : nombrePropiedades) {
			nuevaRaiz = nuevaRaiz.get(propiedad);
		}
		return nuevaRaiz;
	}

	private static Expression<String> filtrarSinAcentos(
			final boolean ignorarAcentos, CriteriaBuilder cb,
			Expression<String> expresion) {
		if (ignorarAcentos) {
			expresion = cb.function("unaccent", String.class, expresion);
		}
		return expresion;
	}

	public static Specification<? extends ObjetoDominio> in(final List<String> nombrePropiedades, final Collection valor) {
		
		return new Specification<ObjetoDominio>() {

			public Predicate toPredicate(Root<ObjetoDominio> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<? extends Object> nuevaRaiz = crearPath(nombrePropiedades,
						root);
				return nuevaRaiz.in(valor);
			}

		};
	}

}