package logisoft.generico.persistencia;

import logisoft.generico.dominio.seguridad.Rol;

import org.springframework.data.repository.CrudRepository;

public interface RepositorioRol extends RepositorioGenerico<Rol, Long> {
	
	public Rol findByNombre(String nombre);

}
