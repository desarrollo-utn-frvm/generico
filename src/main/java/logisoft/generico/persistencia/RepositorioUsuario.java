package logisoft.generico.persistencia;

import java.util.List;

import logisoft.generico.dominio.seguridad.Usuario;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioUsuario extends RepositorioGenerico<Usuario, Long> {
	
	public Usuario findByNombre(String nombre);
	
	@Query("SELECT u FROM Usuario u "
			+ "JOIN u.roles rol "
			+ "WHERE rol.nombre = :rol AND UPPER(u.nombre) like CONCAT('%',UPPER(:texto),'%')")
	public List<Usuario> buscarPorRolConNombre(@Param("rol") String rol,
			@Param("texto") String texto);

}
