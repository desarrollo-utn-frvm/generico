package logisoft.generico.persistencia;

import logisoft.generico.dominio.seguridad.AuditoriaIngreso;

import org.springframework.stereotype.Controller;


@Controller
public interface RepositorioAuditoriaIngreso extends
RepositorioGenerico<AuditoriaIngreso, Long> {

}
