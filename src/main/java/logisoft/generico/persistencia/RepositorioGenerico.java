package logisoft.generico.persistencia;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface RepositorioGenerico<T, ID extends Serializable> extends
		PagingAndSortingRepository<T,ID>, JpaSpecificationExecutor<T> {

	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros);

	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros, Sort orden);
	
	public Iterable<T> buscarGenerico(Map<String, ? extends Object> filtros, Pageable pagina);

}
