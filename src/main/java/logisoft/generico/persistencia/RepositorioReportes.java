package logisoft.generico.persistencia;

import logisoft.generico.dominio.soporte.reporte.ObjetoReporte;

import org.springframework.stereotype.Repository;


@Repository
public interface RepositorioReportes extends
		RepositorioGenerico<ObjetoReporte, Long> {
	
	public ObjetoReporte findByNombreReporte(String nombre);

}
