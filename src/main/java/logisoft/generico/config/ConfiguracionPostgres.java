package logisoft.generico.config;

import javax.sql.DataSource;

import logisoft.generico.persistencia.util.RepositorioGenericoFactoryBean;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "boca.persistencia", 
	entityManagerFactoryRef = "webEntityManager",
	repositoryFactoryBeanClass = RepositorioGenericoFactoryBean.class)
public class ConfiguracionPostgres {
	
	
	@Bean
	@Primary
	@ConfigurationProperties(prefix="spring.datasource.web")
	public DataSource webDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "webEntityManager")
	@Primary
    public LocalContainerEntityManagerFactoryBean webEntityManager(JpaVendorAdapter jpaVendorAdapter) throws Throwable {
		return new EntityManagerFactoryBuilder(jpaVendorAdapter, jpaProperties(), null)
				.dataSource(webDataSource())
				.packages("boca.dominio")
				.persistenceUnit("webPersistenceUnit")
				.build();
    }
	
	
	@Bean(name = "webJpaProperties")
	@Primary
	@ConfigurationProperties(prefix="spring.jpa.web")
	public JpaProperties jpaProperties() {
		return new JpaProperties();
	}

}
