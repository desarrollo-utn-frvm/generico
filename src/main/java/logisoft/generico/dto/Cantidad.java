package logisoft.generico.dto;

public class Cantidad {

	private long cantidad;

	public Cantidad(long cantidad) {
		super();
		this.cantidad = cantidad;
	}

	public long getCantidad() {
		return cantidad;
	}
}