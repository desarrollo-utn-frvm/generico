package logisoft.generico.dto;

import logisoft.generico.dominio.ObjetoDominio;
import lombok.Data;

@Data
public class ObjetoConImagenDTO<T extends ObjetoDominio> {
	private T objeto;
	private byte[] imagen;
	private String tipo;
}
