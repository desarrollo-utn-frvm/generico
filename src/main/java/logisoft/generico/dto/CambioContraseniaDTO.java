package logisoft.generico.dto;

import javax.validation.constraints.Size;

public class CambioContraseniaDTO {
	
	private String contraseniaActual;
	
	@Size(min=6)
	private String contraseniaNueva;

	public String getContraseniaActual() {
		return contraseniaActual;
	}

	public void setContraseniaActual(String contraseniaActual) {
		this.contraseniaActual = contraseniaActual;
	}

	public String getContraseniaNueva() {
		return contraseniaNueva;
	}

	public void setContraseniaNueva(String contraseniaNueva) {
		this.contraseniaNueva = contraseniaNueva;
	}
}
