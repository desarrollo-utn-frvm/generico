package logisoft.generico.logica;

import java.io.IOException;

import javax.transaction.Transactional;

import logisoft.generico.dominio.ObjetoDominio;
import logisoft.generico.dominio.ObjetoImagen;
import logisoft.generico.dto.ObjetoConImagenDTO;
import logisoft.generico.persistencia.RepositorioImagen;
import lombok.SneakyThrows;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;


//@Service
public class ServicioImagen {

	@Autowired
	private RepositorioImagen repositorio;

	@Value("classpath:imagenes/no_photo_icon.png")
	private Resource noFoto;

	@SneakyThrows(IOException.class)
	@Transactional
	public ObjetoImagen getImagen(Long id, String clase) {
		ObjetoImagen imagen = repositorio.findByNombreClaseAndIdObjeto(clase,
				id);

		if (imagen == null || imagen.getImagen() == null) {
			imagen = new ObjetoImagen(IOUtils.toByteArray(noFoto.getInputStream()), "image/png");
		}
		return imagen;
	}

	@SneakyThrows(IOException.class)
	@Transactional
	public boolean guardarImagen(ObjetoConImagenDTO< ? extends ObjetoDominio> obj, Long idObjeto,
			String clase) {
		
		ObjetoImagen objImagen;
		if (obj.getImagen() == null) {
			objImagen= new ObjetoImagen(IOUtils.toByteArray(noFoto.getInputStream()), "image/png");
		}else {
			objImagen = new ObjetoImagen(obj.getImagen(),obj.getTipo()); 
		}

		objImagen.setIdObjeto(idObjeto);
		objImagen.setNombreClase(clase);
		repositorio.save(objImagen);
		return true;
	}

	public boolean actualizarImagen(ObjetoConImagenDTO< ? extends ObjetoDominio> obj, Long idObjeto,
			String clase) {

		ObjetoImagen img = repositorio.findByNombreClaseAndIdObjeto(clase,
				idObjeto);
		img.setImagen(obj.getImagen());
		img.setTipoImagen(obj.getTipo());
		repositorio.save(img);
		return true;
	}
}
