package logisoft.generico.logica;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import logisoft.generico.persistencia.RepositorioGenerico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class ServicioGenerico<T, ID extends Serializable> {

	@Autowired
	protected RepositorioGenerico<T, ID> repositorio;

	@Transactional
	@PreAuthorize("tienePermiso('BUSCAR')")
	public T getItem(ID id) {
		return getItemImp(id);
	}
	
	@Transactional
	protected T getItemImp(ID id) {
		T elemento = repositorio.findOne(id);
		if (elemento == null) {
			throw new RuntimeException("No existe un elemento con ese id");
		}
		return elemento;
	}

	@Transactional
	@PreAuthorize("tienePermiso('LISTAR')")
	public Iterable<T> listarTodos() {
		return listarTodosImp();
	}
	
	@Transactional
	protected Iterable<T> listarTodosImp() {
		return repositorio.findAll();
	}

	@Transactional
	@PreAuthorize("tienePermiso('ACTUALIZAR')")
	public T actualizarItem(T objeto) {
		return actualizarItemImp(objeto);
	}
	
	@Transactional
	protected T actualizarItemImp(T objeto) {
		return repositorio.save(objeto);
	}

	@Transactional
	@PreAuthorize("tienePermiso('GUARDAR')")
	public T guardar(T objeto) {
		return guardarImp(objeto);
	}
	
	@Transactional
	protected T guardarImp(T objeto) {
		return repositorio.save(objeto);
	}

	@Transactional
	@PreAuthorize("tienePermiso('CONTAR')")
	public Long getCantidad() {
		return getCantidadImp();
	}

	@Transactional
	protected Long getCantidadImp() {
		return repositorio.count();
	}
	
	@Transactional
	@PreAuthorize("tienePermiso('LISTAR')")
	public Iterable<T> listarFiltrado(Map<String, ? extends Object> filtros) {
		return listarFiltradoImp(filtros);
	}
	
	@Transactional
	protected Iterable<T> listarFiltradoImp(Map<String, ? extends Object> filtros) {
		return repositorio.buscarGenerico(filtros);
	}
	
	@Transactional
	@PreAuthorize("tienePermiso('LISTAR')")
	public Iterable<T> listarFiltradoYPaginado(Map<String, ? extends Object> filtros, Sort sort) {
		return listarFiltradoYPaginadoImp(filtros, sort);
	}
	
	@Transactional
	protected Iterable<T> listarFiltradoYPaginadoImp(Map<String, ? extends Object> filtros, Sort sort) {
		return repositorio.buscarGenerico(filtros, sort);
	}
	
	@Transactional
	@PreAuthorize("tienePermiso('LISTAR')")
	public Iterable<T> listarFiltradoYPaginado(Map<String, ? extends Object> filtros, Pageable pagina) {
		return listarFiltradoYPaginadoImp(filtros, pagina);
	}
	
	@Transactional
	protected Iterable<T> listarFiltradoYPaginadoImp(Map<String, ? extends Object> filtros, Pageable pagina) {
		return repositorio.buscarGenerico(filtros, pagina);
	}

	@Transactional
	@PreAuthorize("tienePermiso('LISTAR')")
	public Iterable<T> listarPaginado(int pagina, int cantidad, String filtro,
			String campoOrden, boolean reversoOrden) {
		return listarPaginadoImp(pagina, cantidad, filtro, campoOrden, reversoOrden);
	}

	@Transactional
	protected Iterable<T> listarPaginadoImp(int pagina, int cantidad, String filtro,
			String campoOrden, boolean reversoOrden) {
		Sort sort = getOrden(campoOrden, reversoOrden);
		return repositorio.findAll(new PageRequest(pagina, cantidad, sort)).getContent();
	}
	
	@Transactional
	@PreAuthorize("tienePermiso('LISTAR')")
	public Iterable<T> listarPaginado(Sort sort) {
		return listarPaginadoImp(sort);
	}
	
	@Transactional
	protected Iterable<T> listarPaginadoImp(Sort sort) {
		return repositorio.findAll(sort);
	}
	
	@Transactional
	@PreAuthorize("tienePermiso('LISTAR')")
	public Iterable<T> listarPaginado(Pageable pagina) {
		return listarPaginadoImp(pagina);
	}
	
	@Transactional
	protected Iterable<T> listarPaginadoImp(Pageable pagina) {
		return repositorio.findAll(pagina);
	}

	protected Sort getOrden(String campo, boolean reverso) {
		String campoReal = getCampoOrden(campo);
		List<Sort.Order> ordenes = new ArrayList<Sort.Order>();
		Sort.Order ordenOriginal = new Sort.Order(getReversoOrden(reverso),
				getCampoOrden(campoReal));
		ordenes.add(ordenOriginal);
		if (!campoReal.equals("id")) {
			ordenes.add(new Sort.Order(Sort.Direction.ASC, "id"));
		}
		return new Sort(ordenes);
	}

	protected Sort.Direction getReversoOrden(boolean reversoOrden) {
		if (reversoOrden) {
			return Sort.Direction.DESC;
		} else {
			return Sort.Direction.ASC;
		}
	}

	protected String getCampoOrden(String campo) {
		if (campo == null || campo.trim().equals("")) {
			return "codigoNumerico";
		} else {
			return campo;
		}
	}

	@Transactional
	@PreAuthorize("tienePermiso('ELIMINAR')")
	public T eliminar(ID id) {
		return eliminarImp(id);
	}

	@Transactional
	protected T eliminarImp(ID id) {
		T objeto = repositorio.findOne(id);
		if (objeto == null) throw new RuntimeException("El objeto ya existe");
		repositorio.delete(objeto);
		return objeto;
	}

}

