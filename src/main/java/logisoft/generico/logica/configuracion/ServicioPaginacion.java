package logisoft.generico.logica.configuracion;

import logisoft.generico.dominio.web.configuracion.Paginacion;
import logisoft.generico.persistencia.configuracion.RepositorioPaginacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;



@Service
public class ServicioPaginacion {
	@Autowired
	private RepositorioPaginacion repositorio;

	@Value("${config.paginacion.itemsPorPagina}")
	private int itemsPorPagina;
	@Value("${config.paginacion.itemsPorPaginaTransitorio}")
	private int itemsPorPaginaTransitorio;
	@Value("${config.paginacion.tamaniomaximo}")
	private int tamanioMaximo;

	public Paginacion getPaginacion() {
		Iterable<Paginacion> i = repositorio.findAll();
		Paginacion p = null;
		if (i != null) {
			if (i.iterator().hasNext()) {
				p = repositorio.findAll().iterator().next();
			}
		}
		p = validarConfiguracionPaginacionExistente(p);
		repositorio.save(p);
		return p;
	}

	private Paginacion validarConfiguracionPaginacionExistente(Paginacion p) {
		if (p == null) {
			p = new Paginacion();
			p.setItemsPorPagina(itemsPorPagina);
			p.setItemsPorPaginaTransitorio(itemsPorPaginaTransitorio);
			p.setTamanioMaximo(tamanioMaximo);
		}
		return p;
	}
}
