package logisoft.generico.logica.seguridad;

import javax.annotation.PostConstruct;

import logisoft.generico.dominio.seguridad.Permiso;
import logisoft.generico.dominio.seguridad.Rol;
import logisoft.generico.dominio.seguridad.Usuario;
import logisoft.generico.persistencia.RepositorioPermiso;
import logisoft.generico.persistencia.RepositorioRol;
import logisoft.generico.persistencia.RepositorioUsuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ServicioCreacionAdministrador {
	
	@Autowired
	private RepositorioUsuario repositorio;

	@Autowired
	private RepositorioRol repositorioRol;
	
	@Autowired
	private RepositorioPermiso repositorioPermiso;
	
	@Value("${security.user.role}")
	private String nombreRol;
	
	@Value("${security.user.name}")
	private String nombreAdministrador;

	@Value("${security.user.password}")
	private String passwordAdministrador;



	public Rol getRolAdministrador() {
		Rol rolAdministrador = repositorioRol.findByNombre(nombreRol);
		if (rolAdministrador == null) {
			Permiso global = getPermisoGlobal();
			rolAdministrador = new Rol(nombreRol, "");
			rolAdministrador.getPermisos().add(global);
			return repositorioRol.save(rolAdministrador);
		}
		return rolAdministrador;
	}
	
	private Permiso getPermisoGlobal() {
		Permiso permisoGlobal = repositorioPermiso.findByNombre("Administrador");
		if (permisoGlobal == null) {
			permisoGlobal = new Permiso("Administrador","");
			return repositorioPermiso.save(permisoGlobal);
		}
		return permisoGlobal;
	}

	@PostConstruct
	public void validarAdministradorExistente() {
		Usuario administrador = getAdministrador();
		if (administrador.getRoles().size() == 0) {
			administrador.getRoles().add(getRolAdministrador());
			repositorio.save(administrador);
		}
		
		
	}
	public Usuario getAdministrador() {
		Usuario administrador = repositorio.findByNombre(nombreAdministrador);
		if (administrador == null) {
			Usuario usuario = new Usuario(nombreAdministrador,
					passwordAdministrador);
			usuario.getRoles().add(getRolAdministrador());
			administrador = repositorio.save(usuario);
		}
		return administrador;
	}

}
