package logisoft.generico.logica.seguridad;

import javax.transaction.Transactional;

import logisoft.generico.dominio.seguridad.Permiso;
import logisoft.generico.persistencia.RepositorioPermiso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServicioPermisosIniciales {

	@Autowired
	private RepositorioPermiso repositorio;

	public ServicioPermisosIniciales() {
		super();
	}

	public ServicioPermisosIniciales(RepositorioPermiso repositorio) {
		super();
		this.repositorio = repositorio;
	}

	@Transactional
	public Permiso buscarPorNombre(String nombre) {
		Permiso p = repositorio.findByNombre(nombre);
		inicializarPermiso(p);
		return p;
	}

	private void inicializarPermiso(Permiso p) {
		if (p != null) {
			for (Permiso padre : p.getPadres()) {
				inicializarPermiso(padre);
			}
		}
	}

}
