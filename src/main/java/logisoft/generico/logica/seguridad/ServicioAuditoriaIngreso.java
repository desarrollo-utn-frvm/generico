package logisoft.generico.logica.seguridad;

import java.util.Date;

import logisoft.generico.dominio.seguridad.AuditoriaIngreso;
import logisoft.generico.persistencia.RepositorioAuditoriaIngreso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ServicioAuditoriaIngreso {
	
	@Autowired
	private RepositorioAuditoriaIngreso repositorio;
	
	public void registrarInicioSesion(String url, String metodo, String usuario, int estado, String ip) {
		AuditoriaIngreso evento = new AuditoriaIngreso(metodo + " " + url, new Date(), usuario, estado, ip);
		repositorio.save(evento);
	}

}
