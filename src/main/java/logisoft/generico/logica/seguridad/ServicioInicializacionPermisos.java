package logisoft.generico.logica.seguridad;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import logisoft.generico.dominio.seguridad.Permiso;
import logisoft.generico.dominio.seguridad.Rol;
import logisoft.generico.dominio.seguridad.Usuario;
import logisoft.generico.persistencia.RepositorioPermiso;
import logisoft.generico.persistencia.RepositorioRol;
import logisoft.generico.persistencia.RepositorioUsuario;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

//@Service
@Slf4j
public class ServicioInicializacionPermisos {

	private Pattern pattern = Pattern.compile(".*tienePermiso\\('(.*)'\\).*");

	private Pattern prefijo = Pattern
			.compile("ar\\.com\\.carlospazentumano\\.negociosMovil\\.logica\\.(.*)");

	private String permisoPadre = "tienePermiso()";

	@Autowired
	private RepositorioUsuario repositorio;

	@Autowired
	private RepositorioRol repositorioRol;

	@Autowired
	private RepositorioPermiso repositorioPermiso;

	@Autowired
	private ServicioCreacionAdministrador servicioCreacionAdministrador;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	// @Autowired
	// private ConfiguracionPermisosAplicacion config;

	@PostConstruct
	public void incializarPermisos() {
		inicializarPermisos();
	}

	public void inicializarPermisos() {
		try {
			ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(
					false);
			scanner.addIncludeFilter(new AnnotationTypeFilter(
					PreAuthorize.class));
			for (BeanDefinition bd : scanner.findCandidateComponents("")) {
				final Class<?> clase;
				clase = Class.forName(bd.getBeanClassName());
				final PreAuthorize pre = AnnotationUtils.findAnnotation(clase,
						PreAuthorize.class);
				if (pre != null) {
					TransactionTemplate tmpl = new TransactionTemplate(
							txManager);
					tmpl.execute(new TransactionCallbackWithoutResult() {
						@Override
						protected void doInTransactionWithoutResult(
								TransactionStatus status) {
							inicializarPermisos(pre, clase);
						}
					});

				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void inicializarPermisos(PreAuthorize pre, Class<?> clase) {
		if (!permisoPadre.equals(pre.value()))
			return;
		String nombrePadre = clase.getName();
		Matcher matcherPadre = prefijo.matcher(nombrePadre);
		if (!matcherPadre.matches())
			return;
		nombrePadre = matcherPadre.group(1);
		Usuario administrador = servicioCreacionAdministrador.getAdministrador();
		Rol rolAdministrador = servicioCreacionAdministrador.getRolAdministrador();
		if (!administrador.getRoles().contains(rolAdministrador)) {
			administrador.getRoles().add(rolAdministrador);
			repositorio.save(administrador);
		}
		Method[] metodos = clase.getMethods();
		for (Method m : metodos) {
			PreAuthorize p = AnnotationUtils.getAnnotation(m,
					PreAuthorize.class);
			if (p == null)
				continue;
			Matcher matcher = pattern.matcher(p.value());
			if (!matcher.matches())
				continue;
			String nombrePermiso = clase.getName() + '.' + matcher.group(1);
			Matcher matcherPrefijo = prefijo.matcher(nombrePermiso);
			if (!matcherPrefijo.matches())
				continue;
			nombrePermiso = matcherPrefijo.group(1);
			if (repositorioPermiso.findByNombre(nombrePermiso) == null) {
				Permiso permiso = new Permiso(nombrePermiso, "");
				Permiso padre = repositorioPermiso.findByNombre(nombrePadre);
				if (padre == null) {
					padre = new Permiso(nombrePadre, "");
					repositorioPermiso.save(padre);
				}
				permiso.getPadres().add(padre);
				repositorioPermiso.save(permiso);
				if (!rolAdministrador.getPermisos().contains(padre)) {
					rolAdministrador.getPermisos().add(padre);
				}
			}
		}
		repositorioRol.save(rolAdministrador);
	}

}
