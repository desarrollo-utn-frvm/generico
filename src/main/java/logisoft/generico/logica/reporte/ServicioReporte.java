package logisoft.generico.logica.reporte;

import javax.transaction.Transactional;

import logisoft.generico.dominio.soporte.reporte.ObjetoColumnaReporte;
import logisoft.generico.dominio.soporte.reporte.ObjetoReporte;
import logisoft.generico.logica.ServicioGenerico;

import org.springframework.stereotype.Service;

@Service
public class ServicioReporte extends ServicioGenerico<ObjetoReporte, Long> {

	public Iterable<ObjetoColumnaReporte> listarDetalles(Long id) {
		ObjetoReporte r = repositorio.findOne(id);
		return r.getColumnas();
	}

	@Transactional
	public boolean guardarDetalleAcuerdoComercial(Long id,
			ObjetoColumnaReporte nuevoDetalle) {
		ObjetoReporte ac = repositorio.findOne(id);
		ac.getColumnas().add(nuevoDetalle);
		repositorio.save(ac);
		return true;
	}

	public boolean actualizarDetalleAcuerdoComercial(Long id,
			ObjetoColumnaReporte actualizarDetalle) {
		ObjetoReporte ac = repositorio.findOne(id);
		for (ObjetoColumnaReporte dac : ac.getColumnas()) {
			if (dac.getId().equals(actualizarDetalle.getId())) {
				dac.setAlineacionCampo(actualizarDetalle.getAlineacionCampo());
				dac.setAncho(actualizarDetalle.getAncho());
				dac.setMetodo(actualizarDetalle.getMetodo());
				dac.setTamanioFuente(actualizarDetalle.getTamanioFuente());
				dac.setTituloEncabezado(actualizarDetalle.getTituloEncabezado());
			}
		}
		repositorio.save(ac);
		return true;
	}

	public boolean eliminarDetalleAcuerdoComercial(Long idAcuerdo,
			Long idDetalle) {
		ObjetoReporte ac = repositorio.findOne(idAcuerdo);
		ObjetoColumnaReporte det = null;
		for (ObjetoColumnaReporte dac : ac.getColumnas()) {
			if (dac.getId().equals(idDetalle)) {
				det = dac;
			}
		}
		if (det != null) {
			ac.getColumnas().remove(det);
			repositorio.save(ac);
		}
		return true;
	}

}
