package logisoft.generico.logica.reporte;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import logisoft.generico.dominio.soporte.reporte.AlineacionCampoReporte;
import logisoft.generico.dominio.soporte.reporte.ObjetoColumnaReporte;
import logisoft.generico.dominio.soporte.reporte.ObjetoReporte;
import logisoft.generico.persistencia.RepositorioReportes;
import logisoft.generico.util.ConstructorReporte;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

@Service
@Slf4j
@Data
//@AllArgsConstructor
//@NoArgsConstructor
public class ServicioReporteGenerico {

	@Autowired
	private RepositorioReportes repositorio;
	@Autowired
	private ConstructorReporte constructorReporte;
	
	private Resource reporteBase;
	
	public ServicioReporteGenerico(Resource reporteBase) {
		this.reporteBase = reporteBase;
	}

	public ObjetoReporte getReportePorNombre(String nombre) {
		return repositorio.findByNombreReporte(nombre);
	}

	// public byte[] imprimir(OrientacionReporte orientacion, Class<?> clase,
	// Iterable<?> detalles,
	// ArrayList<ObjetoColumnaReporte> columnas, String titulo, String filtro) {
	public byte[] imprimir(ObjetoReporte reporte, Iterable<?> detalles) {

		try {
			int anchoColumnas = Integer.valueOf(reporte.getOrientacion()
					.getAnchoColumna());

			InputStream is = reporteBase.getInputStream();
			String texto = IOUtils.toString(is);
			// Orientacion
			texto = texto.replace("[ORIENTACION]", reporte.getOrientacion()
					.getNombre());
			texto = texto.replace("[ANCHO]", reporte.getOrientacion()
					.getAncho());
			texto = texto.replace("[ALTO]", reporte.getOrientacion().getAlto());
			texto = texto.replace("[ANCHO_COLUMNA]", reporte.getOrientacion()
					.getAnchoColumna());
			texto = texto.replace("[POSICIONPAGINA]", ""
					+ (anchoColumnas - 120));
			texto = texto.replace("[POSICIONTOTALPAGINA]", ""
					+ (anchoColumnas - 40));
			texto = texto.replace("[ALTO_DETALLE]",
					"" + reporte.getAlturaBandaDetalle());

			String definicionField = "<field name=\"this\" class=\""
					+ reporte.getClase().toString().replace("class ", "")
					+ "\"><fieldDescription><![CDATA[_THIS]]></fieldDescription></field>";
			texto = texto.replace("[FIELDS]", definicionField);

			// Encabezados
			StringBuilder sbEncabezado = new StringBuilder();
			// Detalle
			StringBuilder sbColumnasDetalle = new StringBuilder();
			int comienzo = 0;

			for (ObjetoColumnaReporte metodo : reporte.getColumnas()) {
				int ancho = (anchoColumnas * metodo.getAncho()) / 100;
				// Encabezado
				sbEncabezado.append("<staticText>");
				sbEncabezado.append("<reportElement x='" + comienzo
						+ "' y='20' width='" + ancho + "' height='25'/>");
				sbEncabezado
						.append("<textElement verticalAlignment='Middle' textAlignment='Center'/>");
				sbEncabezado.append("<text><![CDATA["
						+ metodo.getTituloEncabezado() + "]]></text>");
				sbEncabezado.append("</staticText>");

				// Detalle
				sbColumnasDetalle.append("<textField isBlankWhenNull='true'>");
				sbColumnasDetalle.append("<reportElement x='" + comienzo
						+ "' y='0' width='" + ancho + "' height='"
						+ reporte.getAlturaBandaDetalle() + "'/>");
				if (metodo.getAlineacionCampo() != AlineacionCampoReporte.CENTRADO) {
					sbColumnasDetalle.append("<box leftPadding='3'/>");
				}
				sbColumnasDetalle
						.append("<textElement verticalAlignment='Middle' textAlignment='"
								+ metodo.getAlineacionCampo() + "'>");
				// <font size="8"/>
				sbColumnasDetalle.append("<font size='"
						+ metodo.getTamanioFuente() + "'/>");
				sbColumnasDetalle.append("</textElement>");
				sbColumnasDetalle.append("<textFieldExpression>");
				sbColumnasDetalle.append("<![CDATA[$F{this}."
						+ metodo.getMetodo() + "]]>");
				sbColumnasDetalle.append("</textFieldExpression>");
				sbColumnasDetalle.append("</textField>");
				comienzo = comienzo + ancho + 1;
			}
			texto = texto.replace("[CAMPOS]", sbColumnasDetalle.toString());
			texto = texto.replace("[ENCABEZADO]", sbEncabezado.toString());
			log.debug(texto);
			Resource resource = new ByteArrayResource(texto.getBytes("UTF-8"));
			JasperReport reporteJR = constructorReporte.getReporte(resource);
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("titulo", reporte.getTitulo());
			parametros.put("filtro", reporte.getFiltro());
			JRBeanCollectionDataSource datos = new JRBeanCollectionDataSource(
					Lists.newArrayList(detalles));
			return constructorReporte.ejecutarReportePDF(reporteJR, parametros,
					datos);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		}
		return null;
	}

	

}
